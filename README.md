# Barebones Static Content

## Quickstart
1. Remove `.tmpl` extension from `.gtlab-ci.yml`
2. Add in the gitlab oauth app id in the `admin/config.yml`.
3. Paste in a few collection configurations.
4. Commit and wait for new page to load. 


## Structure

Folders and files to leave out of the final website:
- `admin/`
- `README.md`
- `.gitlab-ci.yml`


## Deploy Token
```
https://gitlab+deploy-token-70767:w4DCEwaeQ3k4gzKhMKBJ@gitlab.com/ziinc/barebones-static-content.git
```